from flask import Flask, redirect, url_for
from flask_bootstrap import Bootstrap
from flask_dropzone import Dropzone
from flask_nav import Nav


bootstrap = Bootstrap()
dropzone = Dropzone()
nav = Nav()


def create_app():
    app = Flask(__name__)

    bootstrap.init_app(app)
    app.config['BOOTSTRAP_SERVE_LOCAL'] = True

    dropzone.init_app(app)
    app.config['DROPZONE_ALLOWED_FILE_CUSTOM'] = True
    app.config['DROPZONE_ALLOWED_FILE_TYPE'] = 'text/*'

    from .http import register_error_handlers
    register_error_handlers(app)

    from .navbar import top_nav
    nav.init_app(app)
    nav.register_element('top_nav', top_nav)

    from .home import home
    app.register_blueprint(home, url_prefix='/home')

    @app.route('/')
    def index():
        return redirect(url_for('home.index'))

    return app
